package org.example.avro.generic;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecordBuilder;

import java.util.logging.Logger;

public class GenericRecord {
    private static Logger logger = Logger.getLogger(GenericRecord.class.getName());

    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String AGE = "age";
    private static final String HEIGHT = "height";
    private static final String WEIGHT = "weight";
    private static final String AUTOMATED_EMAIL = "automated_email";

    public static void main(String[] args) {
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse("{\n" +
                "     \"type\": \"record\",\n" +
                "     \"namespace\": \"com.example\",\n" +
                "     \"name\": \"Customer\",\n" +
                "     \"fields\": [\n" +
                "       { \"name\": \"first_name\", \"type\": \"string\", \"doc\": \"First Name of Customer\" },\n" +
                "       { \"name\": \"last_name\", \"type\": \"string\", \"doc\": \"Last Name of Customer\" },\n" +
                "       { \"name\": \"age\", \"type\": \"int\", \"doc\": \"Age at the time of registration\" },\n" +
                "       { \"name\": \"height\", \"type\": \"float\", \"doc\": \"Height at the time of registration in cm\" },\n" +
                "       { \"name\": \"weight\", \"type\": \"float\", \"doc\": \"Weight at the time of registration in kg\" },\n" +
                "       { \"name\": \"automated_email\", \"type\": \"boolean\", \"default\": true, \"doc\": \"Field indicating if the user is enrolled in marketing emails\" }\n" +
                "     ]\n" +
                "}");

        GenericRecordBuilder customerBuilder = new GenericRecordBuilder(schema);
        customerBuilder.set(FIRST_NAME, "John");
        customerBuilder.set(LAST_NAME, "Doe");
        customerBuilder.set(AGE, 26);
        customerBuilder.set(HEIGHT, 175f);
        customerBuilder.set(WEIGHT, 70.5f);
        customerBuilder.set(AUTOMATED_EMAIL, false);
        GenericData.Record myCustomer = customerBuilder.build();
        String msg1 = myCustomer.toString();
        logger.info(msg1);

        GenericRecordBuilder customerBuilderWithDefault = new GenericRecordBuilder(schema);
        customerBuilderWithDefault.set(FIRST_NAME, "John");
        customerBuilderWithDefault.set(LAST_NAME, "Doe");
        customerBuilderWithDefault.set(AGE, 26);
        customerBuilderWithDefault.set(HEIGHT, 175f);
        customerBuilderWithDefault.set(WEIGHT, 70.5f);
        GenericData.Record customerWithDefault = customerBuilderWithDefault.build();
        String msg2 = customerWithDefault.toString();
        logger.info(msg2);

        GenericRecordBuilder customerWrong = new GenericRecordBuilder(schema);
        customerWrong.set(HEIGHT, "blahblah");
        customerWrong.set(LAST_NAME, "Doe");
        customerWrong.set(AGE, 26);
        customerWrong.set(WEIGHT, 26f);
        customerWrong.set(AUTOMATED_EMAIL, 70);
        try {
            GenericData.Record wrong = customerWrong.build();
            String msg3 = wrong.toString();
            logger.info(msg3);
        } catch (AvroRuntimeException e){
            logger.info("Generic Record build did not succeed");
            e.printStackTrace();
        }

    }
}
