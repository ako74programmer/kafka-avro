package org.example.avro.kafka.v1;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.example.v1.Customer;

import java.util.Properties;
import java.util.logging.Logger;

public class KafkaAvroJavaProducer {
    static Logger logger = Logger.getLogger(KafkaAvroJavaProducer.class.getName());

    public static void main(String[] args) {
        Properties properties = new Properties();
        // normal producer
        properties.setProperty("bootstrap.servers", "127.0.0.1:9092");
        properties.setProperty("acks", "all");
        properties.setProperty("retries", "10");
        // avro part
        properties.setProperty("key.serializer", StringSerializer.class.getName());
        properties.setProperty("value.serializer", KafkaAvroSerializer.class.getName());
        properties.setProperty("schema.registry.url", "http://127.0.0.1:8881");

        properties.put("auto.register.schemas", true);

        KafkaProducer<String, Customer> producer = new KafkaProducer<>(properties);

        String topic = "customer-avro-customer";

        // copied from avro examples
        Customer customer = Customer.newBuilder().setFirstName("John").setLastName("Doe").setAge(34).setHeight(178f)
                .setWeight(75f).setAutomatedEmail(false).build();

        ProducerRecord<String, Customer> producerRecord = new ProducerRecord<>(topic, customer);

        String msg = customer.toString();
        logger.info(msg);

        producer.send(producerRecord, (metadata, exception) -> {
            if (exception == null) {
                logger.info("Success!");
                logger.info(metadata.toString());
            } else {
                exception.printStackTrace();
            }
        });

        producer.flush();
        producer.close();
    }
}
