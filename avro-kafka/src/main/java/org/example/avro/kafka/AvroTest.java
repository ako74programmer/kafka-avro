package org.example.avro.kafka;

import com.google.gson.Gson;
import org.example.v2.Customer;

public class AvroTest {

    private <T> T jsonToObject(String json, Class<T> clazz) {
        return  (T) new Gson().fromJson(json, clazz);
    }

    public static void main(String[] args) {
        AvroTest me = new AvroTest();
        String jsonString = "{\"first_name\": \"John1\", \"last_name\": \"Doe\", \"age\": 34, \"height\": 178.0, \"weight\": 75.0, \"phone_number\": \"(123)-456-7890\", \"email\": \"john.doe@gmail.com\"}";
        Customer customer = me.jsonToObject(jsonString, Customer.class);
        assert customer.getFirstName().equals("John1");
        assert jsonString.equals(customer);
    }
}
