package org.example.avro.kafka.v2;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.example.v2.Customer;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class KafkaAvroJavaProducer {
    private static Logger logger = Logger.getLogger(KafkaAvroJavaProducer.class.getName());

    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();

        // normal producer
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(ProducerConfig.ACKS_CONFIG, "all");
        properties.setProperty(ProducerConfig.RETRIES_CONFIG, "10");

        // avro part
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        properties.setProperty(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://127.0.0.1:8881");

        KafkaProducer<String, Customer> producer = new KafkaProducer<>(properties);

        String topic = "customer-avro-customer";

        Customer customer = Customer.newBuilder()
                .setAge(34)
                .setFirstName("John")
                .setLastName("Doe")
                .setHeight(178f)
                .setWeight(75f)
                .setEmail("john.doe@gmail.com")
                .setPhoneNumber("(123)-456-7890")
                .build();

        ProducerRecord<String, Customer> producerRecord = new ProducerRecord<>(topic, customer);

        logger.info(customer.toString());
        producer.send(producerRecord, (metadata, exception) -> {
            if (exception == null) {
                logger.info(metadata.toString());
            } else {
                exception.printStackTrace();
            }
        });

        producer.flush();
        producer.close();

    }
}
