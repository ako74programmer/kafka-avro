package org.example.avro.kafka.v1;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.example.v1.Customer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.logging.Logger;

public class KafkaAvroJavaConsumer {
    static Logger logger = Logger.getLogger(KafkaAvroJavaConsumer.class.getName());

    public static void main(String[] args) {
        Properties properties = new Properties();
        // normal consumer
        properties.setProperty("bootstrap.servers","127.0.0.1:9092");
        properties.put("group.id", "customer-consumer-group-v1");
        properties.put("auto.commit.enable", "false");
        properties.put("auto.offset.reset", "earliest");

        // avro part (deserializer)
        properties.setProperty("key.deserializer", StringDeserializer.class.getName());
        properties.setProperty("value.deserializer", KafkaAvroDeserializer.class.getName());
        properties.setProperty("schema.registry.url", "http://127.0.0.1:8881");
        properties.setProperty("specific.avro.reader", "true");

        KafkaConsumer<String, Customer> kafkaConsumer = new KafkaConsumer<>(properties);
        String topic = "customer-avro-customer";
        kafkaConsumer.subscribe(Collections.singleton(topic));

        logger.info("Waiting for data...");

        while (true){
            logger.info("Polling");
            ConsumerRecords<String, Customer> records = kafkaConsumer.poll(Duration.ofMillis(1000));

            for (ConsumerRecord<String, Customer> customerRecord : records){
                Customer customer = customerRecord.value();
                String msg = customer.toString();
                logger.info(msg);
            }

            kafkaConsumer.commitSync();
        }
    }
}
